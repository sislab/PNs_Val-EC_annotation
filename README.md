# PNs_Val-EC_annotation

The annotation of Valence and Emotion Carriers on the Personal Narratives at the level of Functional Units. 

Paper:  Can Emotion Carriers Explain Automatic Sentiment Prediction? A Study on Personal Narratives
Authors: Seyed Mahed Mousavi, Gabriel Roccabruna, Aniruddha Tammewar, Steve Azzolin, Giuseppe Riccardi

Affiliation: Signals and Interactive Systems Lab, University of Trento, Italy

Email: {mahed.mousavi, gabriel.roccabruna , giuseppe.riccardi} {at} unitn.it}